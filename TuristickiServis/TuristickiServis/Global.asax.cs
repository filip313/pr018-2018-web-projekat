﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TuristickiServis.Models;

namespace TuristickiServis
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            InitData();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void InitData()
        {
            JsonSerializer deserializer = new JsonSerializer();
            deserializer.DateFormatString = "dd/MM/yyyy";
            List<Korisnik> korisnici;
            using (StreamReader sr = new StreamReader(Server.MapPath(@".\App_Data\korisnici.json")))
            {
                using (JsonReader jr = new JsonTextReader(sr))
                {
                    korisnici = (List<Korisnik>)deserializer.Deserialize(jr, typeof(List<Korisnik>));
                }
            }

            List<Komentar> komentari;
            using (StreamReader sr = new StreamReader(Server.MapPath(@".\App_Data\komentari.json")))
            {
                using (JsonReader jr = new JsonTextReader(sr))
                {
                    komentari = (List<Komentar>)deserializer.Deserialize(jr, typeof(List<Komentar>));
                }
            }

            Application["korisnici"] = korisnici;
            List<Aranzman> aranzmani = new List<Aranzman>();
            List<Rezervacija> rezervacije = new List<Rezervacija>();
            foreach(var korisnik in korisnici)
            {
                if(korisnik.UlogaKorisnika == Uloga.MENADZER)
                {
                    foreach(var aranzman in ((Menadzer)korisnik).aranzmani)
                    {
                        aranzmani.Add(aranzman);
                    }
                }
                else if(korisnik.UlogaKorisnika == Uloga.TURISTA)
                {
                    foreach(var rez in ((Turista)korisnik).rezervacije)
                    {
                        rezervacije.Add(rez);
                    }
                }
            }

            Application["rezervacije"] = rezervacije;
            Application["aranzmani"] = aranzmani;
            Application["komentari"] = komentari;
        }

        public override void Init()
        {
            this.PostAuthenticateRequest += MyPostAuthenticateRequest;
            base.Init();
        }

        void MyPostAuthenticateRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.Url.AbsolutePath.StartsWith("/rest/"))
            {
                System.Web.HttpContext.Current.SetSessionStateBehavior(
                SessionStateBehavior.Required);
            }
        }
    }
}
