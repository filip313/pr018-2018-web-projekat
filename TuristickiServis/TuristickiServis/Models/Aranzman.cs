﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuristickiServis.Models
{
    public class Aranzman
    {
        public string Naziv { get; set; } 
        public TipAranzmana Tip { get; set; }
        public TipPrevoza TipPrevoza { get; set; }
        public string Destinacija { get; set; }
        public DateTime PocetakPutovanja { get; set; }
        public DateTime KrajPutovanja { get; set; }
        public MestoNalazenja Mesto { get; set; }
        public DateTime VremeNalazenja { get; set; }
        public int MaksBrojPutnika { get; set; }
        public string Opis { get; set; }
        public string ProgramPutovanja { get; set; }
        public string PosterAranzmana { get; set; }
        public Smestaj Smestaj { get; set; }

        public bool Obrisan { get; set; } = false;

        public Aranzman()
        {
            Mesto = new MestoNalazenja();
            Smestaj = new Smestaj();
        }
    }
}