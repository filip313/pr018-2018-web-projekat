﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuristickiServis.Models
{
    public enum Pol { MUSKI, ZENSKI };
    public enum Uloga { ADMINISTRATOR, MENADZER, TURISTA };
    public enum TipAranzmana { Nocenje_sa_doruckom, Polupansion, Pun_Pansion, All_Inclusive, Najam_apartmana }
    public enum TipSmestaja { HOTEL, MOTEL, VILA }
    public enum StatusRezervacije { AKTIVNA, OTKAZANA }

    public enum TipPrevoza { AUTOBUS, AVION, AUTOBUS_AVION, INDIVIDUALNO, OSTALO }
}