﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuristickiServis.Models
{
    public class Rezervacija
    {
        public string  Id { get; set; }
        public string Turista { get; set; }
        public StatusRezervacije Status { get; set; }
        public Aranzman Aranzman { get; set; }
        public SmestajnaJedinica Smestaj { get; set; }
        public bool Obrisan { get; set; } = false;

        public Rezervacija() { }
    }
}