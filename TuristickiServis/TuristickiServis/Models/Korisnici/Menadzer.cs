﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuristickiServis.Models
{
    public class Menadzer : Korisnik
    {
        public List<Aranzman> aranzmani;

        public Menadzer()
        {
            aranzmani = new List<Aranzman>();
        }

        public void DodajAranzman(Aranzman ar)
        {
            aranzmani.Add(ar);
        }

        public List<Aranzman> PreuzmiAranzmane()
        {
            return aranzmani;
        }

        public bool IzbrisiAranzman(Aranzman ar)
        {
            return aranzmani.Remove(ar);
        }
    }
}