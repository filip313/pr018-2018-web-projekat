﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuristickiServis.Models
{
    [JsonConverter(typeof(BaseConverter))]
    public abstract class Korisnik
    {
        public string KorisnickoIme { get; set; }
        public string Lozinka { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public Pol PolKorisnika { get; set; }
        public string  Email { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public Uloga UlogaKorisnika { get; set; }
        public bool Obrisan { get; set; } = false;
    }
}