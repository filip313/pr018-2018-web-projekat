﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuristickiServis.Models
{
    public class Turista : Korisnik
    {
        public bool Sumnjiv { get; set; } = false;
        public bool Blokiran { get; set; } = false;
        public List<Rezervacija> rezervacije;

        public Turista()
        {
            rezervacije = new List<Rezervacija>();
        }

        public void DodajRezevaciju(Rezervacija re)
        {
            rezervacije.Add(re);
        }

        public List<Rezervacija> PreuzmiRezervacije()
        {
            return rezervacije;
        }

        public bool IzbrisiAranzman(Rezervacija re)
        {
            return rezervacije.Remove(re);
        }
    }
}