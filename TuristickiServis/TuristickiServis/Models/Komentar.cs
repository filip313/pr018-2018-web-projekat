﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuristickiServis.Models
{
    public class Komentar
    {
        public Turista turista { get; set; }
        public Aranzman aranzman { get; set; }
        public string Tekst { get; set; }
        public double Ocena { get; set; }
        public bool Obrisan { get; set; } = false;
        public bool Odobren { get; set; } = false;
        public string Id { get; set; }

        public Komentar()
        {
            turista = new Turista();
            aranzman = new Aranzman();
        }
    }
}