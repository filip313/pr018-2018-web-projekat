﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuristickiServis.Models
{
    public class Smestaj
    {
        public TipSmestaja TipSmestaja { get; set; }
        public string Naziv { get; set; }
        public int BrojZvezdica { get; set; }
        public bool Bazen { get; set; }
        public bool SpaCentar { get; set; }
        public bool Dostupnost { get; set; }
        public bool WiFi { get; set; }
        public bool Obrisan { get; set; } = false;
        public List<SmestajnaJedinica> smestajneJedinice;

        public Smestaj()
        {
            smestajneJedinice = new List<SmestajnaJedinica>();
        }

        public void SetBrojZvezdica(int broj)
        {
            if(this.TipSmestaja == TipSmestaja.HOTEL)
            {
                BrojZvezdica = broj;
            }
            else
            {
                BrojZvezdica = -1;
            }
        }

        public void DodajSmestajnuJedinicu(SmestajnaJedinica sj)
        {
            smestajneJedinice.Add(sj);
        }

        public bool ObrisiSmestajnuJedinicu(SmestajnaJedinica sj)
        {
            return smestajneJedinice.Remove(sj);
        }
    }
}