﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuristickiServis.Models
{
    public class MestoNalazenja
    {
        public string Adresa { get; set; }
        public string GeografskaDuzina { get; set; }
        public string GeografskaSirina { get; set; }
        public bool Obrisan { get; set; } = false;

        public MestoNalazenja() { }
    }
}