﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuristickiServis.Models
{
    public class SmestajnaJedinica
    {
        public int Id { get; set; }
        public int DozvoljenBrojGostiju { get; set; }
        public bool DozvoljeniLjubimci { get; set; }
        public double Cena { get; set; }
        public bool Obrisan { get; set; } = false;
        public bool Rezervisan { get; set; } = false;
        public SmestajnaJedinica() { }
    }
}