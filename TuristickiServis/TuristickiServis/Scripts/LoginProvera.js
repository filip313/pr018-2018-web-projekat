﻿var username = "";
var password = "";
var ime = "";
var prezime = "";
var pol = 0;
var email = "";
var datum;
var uspesno = [0, 0, 0, 0, 0, 0, 0];
$(document).ready(function () {

    $('#korisnickoIme').on('focusout', proveriKorisnickoIme);
    $('#lozinka').on('focusout', proveriLozinku)
    $('#potvrdaLozinke').on('focusout', potvrdiLozinku);

    $('#ime').on('focusout', proveriIme);

    $('#prezime').on('focusout', proveriPrezime);

    $('#email').on('focusout', proveriEmail);

    $('#datum').on('focusout', proveriDatum);

    $('input[type="radio"]').on('click', function () {
        pol = $(this).val();
    });



});

function proveriKorisnickoIme() {
    username = $('#korisnickoIme').val();

    if (username == "") {
        $('#usernameError').empty().append("Polje ne sme biti prazno").show();
        uspesno[0] = 0;
    }
    else if (!isNaN(parseInt(username.substring(0, 1)))) {
        $('#usernameError').empty().append("Korisnicko ime ne sme poceti brojem").show();
        uspesno[0] = 0;
    }
    else {
        $('#usernameError').empty().hide();
        uspesno[0] = 1;
    }
}

function proveriLozinku() {
    password = $('#lozinka').val();

    if (password == "") {
        $('#passwordError').empty().append("Polje ne sme biti prazno").show();
        uspesno[1] = 0;
    }
    else if (password.length < 8) {
        $('#passwordError').empty().append("Polje mora imati 8 ili vise karaktera").show();
        uspesno[1] = 0;
    }
    else {
        $('#passwordError').empty().hide();
        uspesno[1] = 1;
    }

}

function potvrdiLozinku() {
    var potvrda = $('#potvrdaLozinke').val();

    if (potvrda == "" || potvrda !== password) {
        $('#potvrdaError').empty().append('Lozinke se ne podudaraju').show();
        uspesno[2] = 0;
    } else {
        $('#potvrdaError').empty().hide();
        uspesno[2] = 1;
    }
}

function proveriIme() {
    ime = $('#ime').val();

    if (ime == "") {
        $('#imeError').empty().append("Polje ne sme biti prazno").show();
        uspesno[3] = 0;
    } else if (!isUpperCase(ime.substring(0, 1))) {
        $('#imeError').empty().append("Ime mora poceti velikim pocetnim slovom").show();
        uspesno[3] = 0;
    } else {
        $('#imeError').empty().hide();
        uspesno[3] = 1;
    }
}

function proveriPrezime() {
    prezime = $('#prezime').val();
    if (prezime == "") {
        $('#prezimeError').empty().append("Polje ne sme biti prazno").show();
        uspesno[4] = 0;
    } else if (!isUpperCase(prezime.substring(0, 1))) {
        $('#prezimeError').empty().append("Prezime mora poceti velikim pocetnim slovom").show();
        uspesno[4] = 0;
    } else {
        $('#prezimeError').empty().hide();
        uspesno[4] = 1;
    }
}

function proveriEmail() {
    email = $('#email').val();

    if (!(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email))) {
        $('#emailError').empty().append('Pogresan format email adrese').show();
        uspesno[5] = 0;
    } else {
        $('#emailError').empty().hide();
        uspesno[5] = 1;
    }
}

function proveriDatum() {
    datum = $('#datum').val();
    if (datum == "") {
        $('#datumError').empty().append('Polje ne moze biti prazno').show();
        uspesno[6] = 0;
    }
    var trenutniDatum = new Date();
    var tmp = datum.split('-');
    if (trenutniDatum.getFullYear() - tmp[0] < 18) {
        $('#datumError').empty().append('Menadzer mora imati bar 18 godina').show();
        uspesno[6] = 0;
    } else {
        uspesno[6] = 1;
        $('#datumError').empty().hide();
    }
}
function isUpperCase(string) {
    return string === string.toUpperCase();
}