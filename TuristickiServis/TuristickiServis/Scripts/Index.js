﻿
var sviAranzmani;
var aktivanFlag = true;
var TipAranzmana = ["Nocenje sa doruckom", "Polupansion", "Pun pansion", "All Inclusive", "Najam apartmana"];
var TipSmestaja = ["Hotel", "Motel", "Vila"];
var TipPrevoza = ["Autobus", "Avion", "Autobus + Avion", "Individualno", "Ostalo"];
var mapArray = new ol.Collection();
var ulogovan = false;

function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });


    return indexed_array;
}

$(document).ready(function () {


    $.ajax({
        url: "/rest/user",
        type: "GET",
        complete: function (data) {
            if (data.responseJSON != null) {
                ulogovan = true;
                showLogOut();
                proveraLinkova(data.responseJSON.UlogaKorisnika);
            } else {
                ulogovan = false;
                showLogIn();
            }
        }
    })
    var text = "";
    //$("#logoutButton").hide();
    $.ajax(
        {
            url: "/rest/api",
            type: "GET",
            dataType: "json",
            complete: function (data) {
                sviAranzmani = data.responseJSON;
                dostupni(sviAranzmani);
            }
        });

    $(".nav li").on("click", function () {
        $(".nav li").removeClass("active");
        $(this).addClass("active");

    });

    $("#sviAranzmani").on("click", function () {
        aktivanFlag = false;
        svi(sviAranzmani);
    });
    $("#dostupniAranzmani").on("click", function () {
        aktivanFlag = true;
        dostupni(sviAranzmani);
    });

    function svi(aranzmani) {
        var $div = $("#aranzmani"); //div sa svim aranzmanima
        $div.empty();

        for (var i = 0; i < aranzmani.length; i++) {

            var $wraper = $("<div></div>");
            $wraper.addClass("container");

            // PROVERA I FORMATIRANJE DATUMA I TRAJANJA PUTOVANJA
            var trenutniDatum = new Date();
            trenutniDatum = trenutniDatum.toJSON();
            var flag;
            if (trenutniDatum > aranzmani[i].PocetakPutovanja) {
                flag = true;
                popuniPodacima(i, $wraper, $div, aranzmani[i], flag);
            }

        }
    }

    function dostupni(aranzmani) {

        // DIV SA SVIM ARANZMANIMA
        var $div = $("#aranzmani");
        $div.empty();

        for (var i = 0; i < aranzmani.length; i++) {
            // DIV SA TABELOM SA PODACIMA O ARNAZMANIMA
            var $wraper = $("<div></div>");
            $wraper.addClass("container");

            // PROVERA I FORMATIRANJE DATUMA I TRAJANJA PUTOVANJA
            var trenutniDatum = new Date();
            trenutniDatum = trenutniDatum.toJSON();
            if (trenutniDatum < aranzmani[i].PocetakPutovanja) {
                popuniPodacima(i, $wraper, $div, aranzmani[i]);
            }

        }
    }

    $("#aranzmanDugme").on("click", function () {

        var selektovaniAranzmani = [];

        var nazivAranzmana = $('[name="nazivAranzmana"]').val();
        
            for (var aranzman of sviAranzmani) {
                if (aranzman.Naziv.toLowerCase().search(nazivAranzmana.toLowerCase()) < 0) {
                    
                } else {
                    selektovaniAranzmani.push(aranzman);
                }
            }

        var odPolazak = $('[name="odPolazak"').val();
        var doPolazak = $('[name="doPolazak"').val();
        if (doPolazak != "") {
            doPolazak +="T00:00:00"
        }
        if (odPolazak != "") {
            odPolazak += "T00:00:00"
        }

        var duzina = selektovaniAranzmani.length;
        for (var i = 0; i < duzina; i++) {
            if ((doPolazak != "" && selektovaniAranzmani[i].PocetakPutovanja > doPolazak))   {
                selektovaniAranzmani.splice(i, 1);
                i--;
                duzina = selektovaniAranzmani.length;
                continue;
            }
            if (odPolazak > selektovaniAranzmani[i].PocetakPutovanja) {
                selektovaniAranzmani.splice(i, 1);
                i--;
                duzina = selektovaniAranzmani.length;
            }
        }

        var odPovratak = $('[name="odPovratak"]').val();
        var doPovratak = $('[name="doPovratak"]').val();

        if (odPovratak != "") {
            odPovratak += "T00:00:00";
        }
        if (doPovratak != "") {
            doPovratak += "T00:00:00";
        }
        var duzina = selektovaniAranzmani.length;
        for (var i = 0; i < duzina; i++) {
            if ((doPovratak != "" && selektovaniAranzmani[i].KrajPutovanja > doPovratak)) {
                selektovaniAranzmani.splice(i, 1);
                i--;
                duzina = selektovaniAranzmani.length;
                continue;
            }
            if (odPovratak > selektovaniAranzmani[i].KrajPutovanja) {
                selektovaniAranzmani.splice(i, 1);
                i--;
                duzina = selektovaniAranzmani.length;
            }
        }


        var metodaPrevoza = $('[name="metodaPrevoza"]').val();

        var index = TipPrevoza.indexOf(metodaPrevoza);
        if (index >= 0) {
            duzina = selektovaniAranzmani.length;
            for (var i = 0; i < duzina; i++) {
                if (index != selektovaniAranzmani[i].TipPrevoza) {
                    selektovaniAranzmani.splice(i, 1);
                    i--;
                    duzina = selektovaniAranzmani.length;
                }
            }
        }
        
        var tipAranzmana = $('[name="tipAranzmana"]').val();

        var index = TipAranzmana.indexOf(tipAranzmana);
        if (index >= 0) {
            duzina = selektovaniAranzmani.length;
            for (var i = 0; i < duzina; i++) {
                if (index != selektovaniAranzmani[i].Tip) {
                    selektovaniAranzmani.splice(i, 1);
                    i--;
                    duzina = selektovaniAranzmani.length;
                }
            }
        }

        if (aktivanFlag) {
            dostupni(selektovaniAranzmani);
        } else {
            svi(selektovaniAranzmani);
        }
    });

    $("#sortirajAranzman").on("click", function () {
        var sortId = $('[name="aranzmanSort"]').val();
        var sortiraniAranzmani = [];
        var preskoci = false;
        switch (sortId) {
            case "0":
                sortiraniAranzmani.push(sviAranzmani[0]);
                for (var i = 1; i < sviAranzmani.length; i++) {
                    preskoci = false;
                    for (var ii = 0; ii < sortiraniAranzmani.length; ii++) {
                        if (sortiraniAranzmani[ii].Naziv > sviAranzmani[i].Naziv) {
                            sortiraniAranzmani.splice(ii, 0, sviAranzmani[i]);
                            preskoci = true;
                            break;
                        }
                    }
                    if (!preskoci) {
                        sortiraniAranzmani.push(sviAranzmani[i]);
                    }
                }
                break;
            case "1":
                sortiraniAranzmani.push(sviAranzmani[0]);
                for (var i = 1; i < sviAranzmani.length; i++) {
                    preskoci = false;
                    for (var ii = 0; ii < sortiraniAranzmani.length; ii++) {
                        if (sortiraniAranzmani[ii].Naziv < sviAranzmani[i].Naziv) {
                            sortiraniAranzmani.splice(ii, 0, sviAranzmani[i]);
                            preskoci = true;
                            break;
                        }
                    }
                    if (!preskoci) {
                        sortiraniAranzmani.push(sviAranzmani[i]);
                    }
                }
                break;
            case "2":
                sortiraniAranzmani.push(sviAranzmani[0]);
                for (var i = 1; i < sviAranzmani.length; i++) {
                    preskoci = false;
                    for (var ii = 0; ii < sortiraniAranzmani.length; ii++) {
                        if (sortiraniAranzmani[ii].PocetakPutovanja > sviAranzmani[i].PocetakPutovanja) {
                            sortiraniAranzmani.splice(ii, 0, sviAranzmani[i]);
                            preskoci = true;
                            break;
                        }
                    }
                    if (!preskoci) {
                        sortiraniAranzmani.push(sviAranzmani[i]);
                    }
                }
                break;
            case "3":
                sortiraniAranzmani.push(sviAranzmani[0]);
                for (var i = 1; i < sviAranzmani.length; i++) {
                    preskoci = false;
                    for (var ii = 0; ii < sortiraniAranzmani.length; ii++) {
                        if (sortiraniAranzmani[ii].PocetakPutovanja < sviAranzmani[i].PocetakPutovanja) {
                            sortiraniAranzmani.splice(ii, 0, sviAranzmani[i]);
                            preskoci = true;
                            break;
                        }
                    }
                    if (!preskoci) {
                        sortiraniAranzmani.push(sviAranzmani[i]);
                    }
                }
                break;
            case "4":
                sortiraniAranzmani.push(sviAranzmani[0]);
                for (var i = 1; i < sviAranzmani.length; i++) {
                    preskoci = false;
                    for (var ii = 0; ii < sortiraniAranzmani.length; ii++) {
                        if (sortiraniAranzmani[ii].KrajPutovanja > sviAranzmani[i].KrajPutovanja) {
                            sortiraniAranzmani.splice(ii, 0, sviAranzmani[i]);
                            preskoci = true;
                            break;
                        }
                    }
                    if (!preskoci) {
                        sortiraniAranzmani.push(sviAranzmani[i]);
                    }
                }
                break;
            case "5":
                sortiraniAranzmani.push(sviAranzmani[0]);
                for (var i = 1; i < sviAranzmani.length; i++) {
                    preskoci = false;
                    for (var ii = 0; ii < sortiraniAranzmani.length; ii++) {
                        if (sortiraniAranzmani[ii].KrajPutovanja < sviAranzmani[i].KrajPutovanja) {
                            sortiraniAranzmani.splice(ii, 0, sviAranzmani[i]);
                            preskoci = true;
                            break;
                        }
                    }
                    if (!preskoci) {
                        sortiraniAranzmani.push(sviAranzmani[i]);
                    }
                }
                break;
            default:
                for (var i in sviAranzmani) {
                    sortiraniAranzmani.push(sviAranzmani[i]);
                }
                break;
        }

        if (aktivanFlag) {
            dostupni(sortiraniAranzmani);
        } else {
            svi(sortiraniAranzmani);
        }
    });

    $("#pretraziJedinice").on("click", function () {
        var odCena = $('[name="odCena"]').val();
        var doCena = $('[name="doCena"]').val();
        var odGostiju = $('[name="odGostiju"]').val();
        var doGostiju = $('[name="doGostiju"]').val();
        var ljubimci = $('[name="ljubimci"]');

        var pretrazeniAranzmani = JSON.parse(JSON.stringify(sviAranzmani));
        

        if (odCena != "") {
            var duzina = pretrazeniAranzmani.length;
            for (var i = 0; i < duzina; i++) {
                var duzinaSmestaj = pretrazeniAranzmani[i].Smestaj.smestajneJedinice.length;
                for (var ii = 0; ii < duzinaSmestaj; ii++) {
                    if (odCena > pretrazeniAranzmani[i].Smestaj.smestajneJedinice[ii].Cena) {
                        pretrazeniAranzmani[i].Smestaj.smestajneJedinice.splice(ii, 1);
                        ii--;
                        duzinaSmestaj = pretrazeniAranzmani[i].Smestaj.smestajneJedinice.length;
                    }
                }
            }
        }

        if (doCena != "") {
            var duzina = pretrazeniAranzmani.length;
            for (var i = 0; i < duzina; i++) {
                var duzinaSmestaj = pretrazeniAranzmani[i].Smestaj.smestajneJedinice.length;
                for (var ii = 0; ii < duzinaSmestaj; ii++) {
                    if (doCena < pretrazeniAranzmani[i].Smestaj.smestajneJedinice[ii].Cena) {
                        pretrazeniAranzmani[i].Smestaj.smestajneJedinice.splice(ii, 1);
                        ii--;
                        duzinaSmestaj = pretrazeniAranzmani[i].Smestaj.smestajneJedinice.length;
                    }
                }
            }
        }

        if (odGostiju != "") {
            var duzina = pretrazeniAranzmani.length;
            for (var i = 0; i < duzina; i++) {
                var duzinaSmestaj = pretrazeniAranzmani[i].Smestaj.smestajneJedinice.length;
                for (var ii = 0; ii < duzinaSmestaj; ii++) {
                    if (odGostiju > pretrazeniAranzmani[i].Smestaj.smestajneJedinice[ii].DozvoljenBrojGostiju) {
                        pretrazeniAranzmani[i].Smestaj.smestajneJedinice.splice(ii, 1);
                        ii--;
                        duzinaSmestaj = pretrazeniAranzmani[i].Smestaj.smestajneJedinice.length;
                    }
                }
            }
        }

        if (doGostiju != "") {
            var duzina = pretrazeniAranzmani.length;
            for (var i = 0; i < duzina; i++) {
                var duzinaSmestaj = pretrazeniAranzmani[i].Smestaj.smestajneJedinice.length;
                for (var ii = 0; ii < duzinaSmestaj; ii++) {
                    if (doGostiju < pretrazeniAranzmani[i].Smestaj.smestajneJedinice[ii].DozvoljenBrojGostiju) {
                        pretrazeniAranzmani[i].Smestaj.smestajneJedinice.splice(ii, 1);
                        ii--;
                        duzinaSmestaj = pretrazeniAranzmani[i].Smestaj.smestajneJedinice.length;
                    }
                }
            }
        }

        if (ljubimci[0].checked) {
            var duzina = pretrazeniAranzmani.length;
            for (var i = 0; i < duzina; i++) {
                var duzinaSmestaj = pretrazeniAranzmani[i].Smestaj.smestajneJedinice.length;
                for (var ii = 0; ii < duzinaSmestaj; ii++) {
                    if (!pretrazeniAranzmani[i].Smestaj.smestajneJedinice[ii].DozvoljeniLjubimci) {
                        pretrazeniAranzmani[i].Smestaj.smestajneJedinice.splice(ii, 1);
                        ii--;
                        duzinaSmestaj = pretrazeniAranzmani[i].Smestaj.smestajneJedinice.length;
                    }
                }
            }
        }

        if (aktivanFlag) {
            dostupni(pretrazeniAranzmani);
        } else {
            svi(pretrazeniAranzmani);
        }
    });


    $('#pretraziSmestaj').on('click', function () {
        var pretrazeniAranzmani = JSON.parse(JSON.stringify(sviAranzmani));
        var tip = $('[name="tipSmestaja"').val();
        var naziv = $('[name="nazivSmestaja"]').val();
        var bazen = $('[name="bazen"]');
        var spa = $('[name="spa"]');
        var dostupno = $('[name="dostupno"]');
        var wifi = $('[name="wifi"]');

        var duzina = pretrazeniAranzmani.length;
        for (var i = 0; i < duzina; i++) {
            if (pretrazeniAranzmani[i].Smestaj.Naziv.toLowerCase().search(naziv.toLowerCase()) < 0) {
                pretrazeniAranzmani.splice(i, 1);
                i--;
                duzina = pretrazeniAranzmani.length;
                continue;
            }
            if (tip != "") {

                if (pretrazeniAranzmani[i].Smestaj.TipSmestaja != tip) {
                    pretrazeniAranzmani.splice(i, 1);
                    i--;
                    duzina = pretrazeniAranzmani.length;
                    continue;
                }
            }
            if (bazen[0].checked) {
                if (!pretrazeniAranzmani[i].Smestaj.Bazen) {
                    pretrazeniAranzmani.splice(i, 1);
                    i--;
                    duzina = pretrazeniAranzmani.length;
                    continue;
                }
            }
            if (spa[0].checked) {
                if (!pretrazeniAranzmani[i].Smestaj.SpaCentar) {
                    pretrazeniAranzmani.splice(i, 1);
                    i--;
                    duzina = pretrazeniAranzmani.length;
                    continue;
                }
            }
            if (dostupno[0].checked) {
                if (!pretrazeniAranzmani[i].Smestaj.Dostupnost) {
                    pretrazeniAranzmani.splice(i, 1);
                    i--;
                    duzina = pretrazeniAranzmani.length;
                    continue;
                }
            }
            if (wifi[0].checked) {
                if (!pretrazeniAranzmani[i].Smestaj.WiFi) {
                    pretrazeniAranzmani.splice(i, 1);
                    i--;
                    duzina = pretrazeniAranzmani.length;
                    continue;
                }
            }
        }

        if (aktivanFlag) {
            dostupni(pretrazeniAranzmani);
        } else {
            svi(pretrazeniAranzmani);
        }
    });


    $('#sortirajSmestaj').on('click', function () {
        var sortiraniAranzmani = JSON.parse(JSON.stringify(sviAranzmani));
        var sortId = $('[name="smestajSort"]').val();
        switch (sortId) {
            case "0":
                sortiraniAranzmani.sort((prvi, drugi) => {
                    if (prvi.Smestaj.Naziv < drugi.Smestaj.Naziv) {
                        return -1;
                    }
                    if (prvi.Smestaj.Naziv > drugi.Smestaj.Naziv) {
                        return 1;
                    }
                    return 0;
                });
                break;
            case "1":
                sortiraniAranzmani.sort((prvi, drugi) => {
                    if (prvi.Smestaj.Naziv > drugi.Smestaj.Naziv) {
                        return -1;
                    }
                    if (prvi.Smestaj.Naziv < drugi.Smestaj.Naziv) {
                        return 1;
                    }
                    return 0;
                });
                break;
            case "2":
                sortiraniAranzmani.sort((prvi, drugi) => {
                    if (prvi.Smestaj.smestajneJedinice.length < drugi.Smestaj.smestajneJedinice.length) {
                        return -1;
                    }
                    if (prvi.Smestaj.smestajneJedinice.length > drugi.Smestaj.smestajneJedinice.length) {
                        return 1;
                    }
                    return 0;
                });
                break;
            case "3":
                sortiraniAranzmani.sort((prvi, drugi) => {
                    if (prvi.Smestaj.smestajneJedinice.length > drugi.Smestaj.smestajneJedinice.length) {
                        return -1;
                    }
                    if (prvi.Smestaj.smestajneJedinice.length < drugi.Smestaj.smestajneJedinice.length) {
                        return 1;
                    }
                    return 0;
                });
                break;
            case "4":
                sortiraniAranzmani.sort((prvi, drugi) => {
                    var prviSlobodni = 0;
                    var drugiSlobodni = 0;
                    for (var i = 0; i < prvi.Smestaj.smestajneJedinice.length; i++) {
                        if (!prvi.Smestaj.smestajneJedinice.Rezervisan) {
                            prviSlobodni++;
                        }
                    }
                    for (var i = 0; i < drugi.Smestaj.smestajneJedinice.length; i++) {
                        if (!drugi.Smestaj.smestajneJedinice.Rezervisan) {
                            drugiSlobodni++;
                        }
                    }

                    if (prviSlobodni < drugiSlobodni) {
                        return -1;
                    }
                    if (prviSlobodni > drugiSlobodni) {
                        return 1;
                    }

                    return 0;

                });
                break;
            case "5":
                sortiraniAranzmani.sort((prvi, drugi) => {
                    var prviSlobodni = 0;
                    var drugiSlobodni = 0;
                    for (var i = 0; i < prvi.Smestaj.smestajneJedinice.length; i++) {
                        if (!prvi.Smestaj.smestajneJedinice.Rezervisan) {
                            prviSlobodni++;
                        }
                    }
                    for (var i = 0; i < drugi.Smestaj.smestajneJedinice.length; i++) {
                        if (!drugi.Smestaj.smestajneJedinice.Rezervisan) {
                            drugiSlobodni++;
                        }
                    }

                    if (prviSlobodni > drugiSlobodni) {
                        return -1;
                    }
                    if (prviSlobodni < drugiSlobodni) {
                        return 1;
                    }

                    return 0;

                });
                break;
        }

        if (aktivanFlag) {
            dostupni(sortiraniAranzmani);
        } else {
            svi(sortiraniAranzmani);
        }
    });

    $("#sortirajJedinice").on("click", function () {
        var sortId = $('[name="jediniceSort"').val();
        var sortiraneJedinice = JSON.parse(JSON.stringify(sviAranzmani));
        var preskoci = false;
        switch (sortId) {
            case "0":
                for (var i = 0; i < sortiraneJedinice.length; i++) {
                    var tmpSort = [];
                    tmpSort[0] = sortiraneJedinice[i].Smestaj.smestajneJedinice[0];
                    for (var ii = 1; ii < sortiraneJedinice[i].Smestaj.smestajneJedinice.length; ii++) {
                        var preskoci = false;
                        for (var iii = 0; iii < tmpSort.length; iii++) {
                            if (tmpSort[iii].DozvoljenBrojGostiju > sortiraneJedinice[i].Smestaj.smestajneJedinice[ii].DozvoljenBrojGostiju) {
                                tmpSort.splice(iii, 0, sortiraneJedinice[i].Smestaj.smestajneJedinice[ii]);
                                preskoci = true;
                                break;
                            }
                        }

                        if (!preskoci) {
                            tmpSort.push(sortiraneJedinice[i].Smestaj.smestajneJedinice[ii]);
                        }
                    }

                    sortiraneJedinice[i].Smestaj.smestajneJedinice = tmpSort;
                }
                break;
            case "1":
                for (var i = 0; i < sortiraneJedinice.length; i++) {
                    var tmpSort = [];
                    tmpSort[0] = sortiraneJedinice[i].Smestaj.smestajneJedinice[0];
                    for (var ii = 1; ii < sortiraneJedinice[i].Smestaj.smestajneJedinice.length; ii++) {
                        var preskoci = false;
                        for (var iii = 0; iii < tmpSort.length; iii++) {
                            if (tmpSort[iii].DozvoljenBrojGostiju < sortiraneJedinice[i].Smestaj.smestajneJedinice[ii].DozvoljenBrojGostiju) {
                                tmpSort.splice(iii, 0, sortiraneJedinice[i].Smestaj.smestajneJedinice[ii]);
                                preskoci = true;
                                break;
                            }
                        }

                        if (!preskoci) {
                            tmpSort.push(sortiraneJedinice[i].Smestaj.smestajneJedinice[ii]);
                        }
                    }

                    sortiraneJedinice[i].Smestaj.smestajneJedinice = tmpSort;
                }
                break;
            case "2":
                for (var i = 0; i < sortiraneJedinice.length; i++) {
                    var tmpSort = [];
                    tmpSort[0] = sortiraneJedinice[i].Smestaj.smestajneJedinice[0];
                    for (var ii = 1; ii < sortiraneJedinice[i].Smestaj.smestajneJedinice.length; ii++) {
                        var preskoci = false;
                        for (var iii = 0; iii < tmpSort.length; iii++) {
                            if (tmpSort[iii].Cena > sortiraneJedinice[i].Smestaj.smestajneJedinice[ii].Cena) {
                                tmpSort.splice(iii, 0, sortiraneJedinice[i].Smestaj.smestajneJedinice[ii]);
                                preskoci = true;
                                break;
                            }
                        }

                        if (!preskoci) {
                            tmpSort.push(sortiraneJedinice[i].Smestaj.smestajneJedinice[ii]);
                        }
                    }

                    sortiraneJedinice[i].Smestaj.smestajneJedinice = tmpSort;
                }
                break;
            case "3":
                for (var i = 0; i < sortiraneJedinice.length; i++) {
                    var tmpSort = [];
                    tmpSort[0] = sortiraneJedinice[i].Smestaj.smestajneJedinice[0];
                    for (var ii = 1; ii < sortiraneJedinice[i].Smestaj.smestajneJedinice.length; ii++) {
                        var preskoci = false;
                        for (var iii = 0; iii < tmpSort.length; iii++) {
                            if (tmpSort[iii].Cena < sortiraneJedinice[i].Smestaj.smestajneJedinice[ii].Cena) {
                                tmpSort.splice(iii, 0, sortiraneJedinice[i].Smestaj.smestajneJedinice[ii]);
                                preskoci = true;
                                break;
                            }
                        }

                        if (!preskoci) {
                            tmpSort.push(sortiraneJedinice[i].Smestaj.smestajneJedinice[ii]);
                        }
                    }

                    sortiraneJedinice[i].Smestaj.smestajneJedinice = tmpSort;
                }
                break;
            default:
                break;
        }

        if (aktivanFlag) {
            dostupni(sortiraneJedinice);
        } else {
            svi(sortiraneJedinice);
        }
    });
});


function login() {

    var formData = getFormData($("#loginForm"))
    var s = JSON.stringify(formData);
    $.ajax({
        url: "/rest/user/Login",
        type: "POST",
        data: s,
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            showLogOut();
            ulogovan = true;
            proveraLinkova(data);
        },
        error: function (data) {
            $('#loginForm label[name="loginError"]').text("Pogresno korisnicko ime ili lozinka");
            
        }
    });
}

function logout() {
    $.ajax({
        url: "/rest/user/Logout",
        type: "GET",
        complete: function (data) {
            showLogIn();
            ulogovan = false;
            $('ul[name="navigacija"]').children().last().remove();
        }
    })
}

function rezervisi() {
    var naziv = this.name;
    var id = parseInt(this.id.split('_')[1]);
    var url = "/rest/jedinica/rezervisi/" + naziv + "/" + id;

    if (confirm('Da li zelite da rezervisete ovaj smestaj?')) {
        $.ajax({
            url: url,
            type: "GET",
            success: function (data) {
                alert('Uspesno rezervisan aranzman');
                window.location.reload();
            },
            error: function (data) {
                if (data.status == 401) {
                    alert('Morate biti ulogovani da bi ste izvrsili rezervaciju')
                } else {
                    alert('Trazeni aranzman ili rezervacija ne postoji');
                }
            }
        })
    }
}

function addLeadingZeros(n) {
    if (n <= 9) {
        return "0" + n;
    }

    return n;
}

function initMap(mapId, Mesto) {
    var i = parseInt(mapId.substring(mapId.length - 1));

    const marker = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([Mesto.GeografskaSirina, Mesto.GeografskaDuzina])));

    mapArray[i] = new ol.Map({
        view: new ol.View({
            center: ol.proj.fromLonLat([Mesto.GeografskaSirina, Mesto.GeografskaDuzina]),
            zoom: 17
        }),
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            }),
            new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: [marker]
                }),
                style: new ol.style.Style({
                    image: new ol.style.Icon({
                        anchor: [0.5, 10],
                        anchorXUnits: 'fraction',
                        anchorYUnits: 'pixels',
                        src: '/Images/marker.png'
                    })
                })
            })
        ],
        target: mapId
    });
}

function popuniPodacima(i, $wraper, $div, text, flag = false) {
    var pocetak = new Date(text.PocetakPutovanja);
    var kraj = new Date(text.KrajPutovanja);
    var trajanje = (kraj - pocetak) / (1000 * 60 * 60 * 24);

    // RACUNANJE MINIMALNE CENE SMESTAJA
    var minCena = Infinity;
    if (text.Smestaj != null) {
        for (var jedinica of text.Smestaj.smestajneJedinice) {
            if (jedinica.Cena < minCena) {
                minCena = jedinica.Cena;
            }
        }
    }
    if (minCena === Infinity) {
        minCena = 0;
    }


    // DUGME ZA DETALJE 
    var $button = $("<button></button>");
    $button.attr({ type: "button", id: "slide" + i });
    $button.text("Detalji");
    $button.addClass("btn btn-success btn-xs");
    var innerid = "#inner" + i;
    $button.on("click", function () {
        var id = parseInt(this.id[this.id.length - 1]);
        var divId = "#inner" + id;
        var mapId = "#mapPanel_" + id;
        $(divId).slideToggle();
        $(mapId).slideToggle();
        mapArray[id].updateSize();

    });

    // VIDLJIVA TABELA SA POCETNIM INFORMACJAMA O ARANZMANU
    var $table = $("<table></table>");
    $table.addClass("table");
    $table.append("<tr><td><b>Polazak:</b></br>" + pocetak.getDate() + "." + (pocetak.getMonth()+1) + "." + pocetak.getFullYear() + "</td><td><b>Povratak:</b> </br>" + kraj.getDate() + "." + (kraj.getMonth()+1) + "." + kraj.getFullYear() + "</td><td><b>Dana:</b> </br>" + trajanje + "</td></tr>");

    var $tdSlika = $("<td></td>");
    $slika = $("<img />");
    $slika.attr({ "src": text.PosterAranzmana, "height": 80, "width": 80 });
    $slika.addClass("img-circle");
    $tdSlika.append($slika);
    $table.children().last().append($tdSlika);

    $table.children().last().append("<td><b>Destinacija:</b></br>" + text.Destinacija + "</td><td><b>od:</b></br> " + minCena + "</td></tr>");

    $tdButton = $("<td></td>").append($button);
    $table.children().last().append($tdButton);
    $wraper.append($table)

    // SAKRIVEN DIV SA DODATNIM INFORMACIJAMA O ARANZMANU
    var $div2 = $("<div></div>");
    var $tr = $("<tr><td colspan=\"3\"> </td></tr>");
    $tr.children().last().append($div2);
    $div2.attr({ id: "inner" + i, class: "panel panel-info" });
    $div2.hide();

    $panelHeading = $("<div></div>");
    $panelHeading.append(text.Naziv);
    $panelHeading.append("</br>" + TipAranzmana[text.Tip]);
    $panelHeading.addClass("panel-heading");
    $div2.append($panelHeading);

    var $panelBody = $("<div></div>");
    $panelBody.addClass("panel-body");

    // TABELA SA OSTALIM INFORMACIJAMA O ARANZMANU 
    var $innerTable = $("<table></table>");
    var $innerTbody = $("<tbody></tbody>")
    $innerTable.addClass("table table-striped");
    var vreme = new Date(text.VremeNalazenja);
    $innerTbody.append("<tr><td colspan=\"3\"><b>Vreme nalazenja:</b></br>" + addLeadingZeros(vreme.getHours()) + ":" + addLeadingZeros(vreme.getMinutes()) + "</td><td><b>Putnika:</b></br>" + text.MaksBrojPutnika + "</td><td><b>Metod prevoza:</b></br>" + TipPrevoza[text.TipPrevoza] + "</td></tr>");
    $innerTbody.append("<tr><td colspan=\"5\">" + text.Opis + "</td></tr>");
    $innerTbody.append("<tr><td colspan=\"5\">" + text.ProgramPutovanja + "</td></tr>");
    $innerTable.append($innerTbody);

    // TABELA SA INFORMACIJAMA O SMESTAJU
    if (text.Smestaj != null) {
        var $smestajTabela = $("<table></table>");
        var $smestajTbody = $("<tbody></tbody>");
        $smestajTabela.addClass("table  table-striped");
        var $vrstaSmestaja = $("<td><b>" + TipSmestaja[text.Smestaj.TipSmestaja] + "</b></td>");
        $vrstaSmestaja.attr({ "colspan": 2 });
        var $nazivSmestaja = $("<td><b>" + text.Smestaj.Naziv + "</b></td>");
        $smestajTbody.append("<tr></tr>");
        $smestajTbody.children().last().append($nazivSmestaja);
        $smestajTbody.children().last().append($vrstaSmestaja);

        if (text.Smestaj.TipSmestaja == 0) {
            $smestajTbody.children().last().append("<td><b>Broj zvezdica:</b></br>" + text.Smestaj.BrojZvezdica + "</td>");
        } else {
            $nazivSmestaja.attr({ "colspan": 2 });
        }
        var bazen = (text.Smestaj.Bazen) ? "da" : "ne";
        var spa = (text.Smestaj.SpaCentar) ? "da" : "ne";
        var dostupnost = (text.Smestaj.Dostupnost) ? "da" : "ne";
        var wifi = (text.Smestaj.WiFi) ? "da" : "ne";
        $smestajTbody.append("<tr><td><b>Bazen:</b></br>" + bazen + "</td><td><b>Spa Centar:</b></br>" + spa + "</td><td><b>Dostupno za invalide:</b></br>" + dostupnost + "</td><td><b>WiFi:</b><br>" + wifi + "</td></tr>s");

        for (var j = 0; j < text.Smestaj.smestajneJedinice.length; j++) {
            var jedinica = text.Smestaj.smestajneJedinice[j];
            var ljubimci = (jedinica.DozvoljeniLjubimci) ? "da" : "ne";
            var $rezervacija = $("<button></button>");
            $rezervacija.attr({ "id": "rezervisi_" + jedinica.Id, "name": text.Naziv });
            if (jedinica.Rezervisan) {

                $rezervacija.addClass("btn btn-secondary btn-xs");
                $rezervacija.append("Rezervisano");
                $rezervacija.prop("disabled", true);
            } else {

                $rezervacija.addClass("btn btn-success btn-xs");
                $rezervacija.append("Rezervisi");
                $rezervacija.on("click", rezervisi);
            }

            if (flag) {
                $rezervacija.prop("disabled", true);
            }

            $smestajTbody.append("<tr><td><b>Sobnost:</b></br>" + jedinica.DozvoljenBrojGostiju + "</td><td><b>Dozvoljeni ljubimci:</b> </br>" + ljubimci + "</td><td><b>Cena:</b></br>" + jedinica.Cena + "</td></tr>");
            var $temp = $("<td></td>");
            $temp.append($rezervacija);
            $smestajTbody.children().last().append($temp);
        }
        $smestajTabela.append($smestajTbody);
        $panelBody.append($smestajTabela);
    }

    $panelBody.prepend($innerTable);
    $div2.append($panelBody);
    var $mapTd = $("<td colspan=\"4\"></td>");

    var $mapPanel = $("<div></div>").addClass("panel panel-info razmak");
    $mapPanel.attr({ "id": "mapPanel_" + i });
    $mapPanel.hide();

    var $mapPanelHeading = $("<div></div>").addClass("panel-heading");
    $mapPanelHeading.append(text.Mesto.Adresa);
    $mapPanel.append($mapPanelHeading);

    var $mapPanelBody = $("<div></div>").addClass("panel-body");

    var $mapDiv = $("<div></div>");
    $mapDiv.attr({ "id": "map_" + i });
    $mapDiv.addClass("map");

    $mapPanelBody.append($mapDiv);
    $mapPanel.append($mapPanelBody);

    $mapTd.append($mapPanel);
    $tr.append($mapTd);
    $table.append($tr);

    var $comentTr = $('<tr><td colspan="7"><div class="panel panel-default komentari"><div class="panel-heading" id="komentar_' + i + '" onclick="ucitajKomentar(\'#komentar_' + i +'\')">Komentari</div><div class="panel-body" hidden><table class="table"></table></div></div></td></tr>')
    $table.append($comentTr);
    $div.append($wraper);

    initMap("map_" + i, text.Mesto);
}

function mojaFunkcija(divId) {
    $(divId).slideToggle();
}

function ucitajKomentar(komentarId) {

    var id = komentarId.substring(komentarId.length - 1);
    var $dete = $("#inner" + id).children().first();
    var naziv = $dete[0].firstChild.data;
    var url = "/rest/komentari/" + naziv;
    var $body = $(komentarId).siblings()[0];
    var $table = $(komentarId).siblings().first().children().first();

    if ($body.hidden) {
        $.ajax({
            url: url,
            type: "GET",
            complete: function (data) {
                $table.empty();
                for (var i in data.responseJSON) {
                    var ocena = data.responseJSON[i].Ocena;
                    var text = data.responseJSON[i].Tekst;
                    var korisnik = data.responseJSON[i].turista.Ime + " " + data.responseJSON[i].turista.Prezime;
                    $table.append('<tr><td>' + (1+parseInt(i)) +'</td><td>' + korisnik +'</td><td colspan="3">' + text + '</td><td>' + ocena + '</td></tr>');
                }
            }
        });

        $body.hidden = false;
    } else {
        $body.hidden = true;
    }
}

function showLogOut() {
    $('#profil').show();
    $('#loginForm label[name="loginError"]').empty();
    $("#loginForm").hide();
    $("#registerButton").hide();
    $("#logoutButton").show();
}

function showLogIn() {
    $('#profil').hide();
    $("#logoutButton").hide();
    $("#loginForm").show();
    $("#registerButton").show();
    $('#loginForm input[name="username"]').val('');
    $('#loginForm input[name="password"]').val('');
}

function proveraLinkova(data) {
    switch (data) {
        case 0:
            $('ul[name="navigacija"]').append('<li><a href="/Pogledi/Admin.html">Administracija</a></li>');
            break;
        case 1:
            $('ul[name="navigacija"]').append('<li><a href="/Pogledi/Aranzmani.html">Uredjivanje Aranzmana</a></li>');
            break;
        case 2:
            $('ul[name="navigacija"]').append('<li><a href="/Pogledi/Rezervacije.html">Vase Rezervacije</a></li>');
            break;
    }
}
