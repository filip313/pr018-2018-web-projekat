﻿using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using TuristickiServis.Models;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;

namespace TuristickiServis.Controllers
{
    [RoutePrefix("rest")]
    public class UserController : ApiController
    {
        [Route("user/promeniPodatke")]
        [HttpPost]
        public IHttpActionResult PromeniPodateke(PromenaPodataka promena)
        {
            var user = HttpContext.Current.Session["user"] as Korisnik;
            if(user != null)
            {
                user.Ime = promena.Ime;
                user.Prezime = promena.Prezime;
                user.Email = promena.Email;
                SaveUsers();
                return Created("user", true);
            }

            return NotFound();
        }
        [Route("user/promenaSifre")]
        [HttpPost]
        public IHttpActionResult PromeniSifru(PromenaSifre promena)
        {
            var user = HttpContext.Current.Session["user"] as Korisnik;
            if(user != null)
            {
                if (user.Lozinka == promena.StaraSifra)
                {
                    user.Lozinka = promena.NovaSifra;
                    SaveUsers();
                    Get();
                    return Created("user", true);
                }
            }

            return NotFound();
        }



        [Route("user")]
        public IHttpActionResult GetLogedInUser()
        {
            var user = HttpContext.Current.Session["user"] as Korisnik;
            Korisnik ret = null;
            if(user != null)
            {
                if (user.UlogaKorisnika == Uloga.ADMINISTRATOR)
                {
                    ret = new Admin();
                    ret.KorisnickoIme = user.KorisnickoIme;
                    ret.UlogaKorisnika = user.UlogaKorisnika;
                    ret.Ime = user.Ime;
                    ret.Prezime = user.Prezime;
                    ret.Email = user.Email;
                    ret.PolKorisnika = user.PolKorisnika;
                    ret.DatumRodjenja = user.DatumRodjenja;
                }else if(user.UlogaKorisnika == Uloga.MENADZER)
                {
                    ret = new Menadzer();
                    ret.KorisnickoIme = user.KorisnickoIme;
                    ret.UlogaKorisnika = user.UlogaKorisnika;
                    ret.Ime = user.Ime;
                    ret.Prezime = user.Prezime;
                    ret.Email = user.Email;
                    ret.PolKorisnika = user.PolKorisnika;
                    ret.DatumRodjenja = user.DatumRodjenja;
                }
                else if(user.UlogaKorisnika == Uloga.TURISTA)
                {
                    ret = new Turista();
                    ret.KorisnickoIme = user.KorisnickoIme;
                    ret.UlogaKorisnika = user.UlogaKorisnika;
                    ret.Ime = user.Ime;
                    ret.Prezime = user.Prezime;
                    ret.Email = user.Email;
                    ret.PolKorisnika = user.PolKorisnika;
                    ret.DatumRodjenja = user.DatumRodjenja;
                }
            }
            else
            {
                ret = user;
            }

            return Ok(ret);
        }
        // GET api/values
        [HttpGet]
        [Route("admin/Users")]
        public IEnumerable<Korisnik> Users()
        {
            var korisnici = (List<Korisnik>)HttpContext.Current.Application["korisnici"];
            var ret = new List<Korisnik>();
            foreach (var korisnik in korisnici)
            {
                if (!korisnik.Obrisan)
                {
                    ret.Add(korisnik);
                }
            }
            return ret;
        }

        [HttpPost]
        [Route("user/Login")]
        public IHttpActionResult Login([FromBody] JObject data)
        {
            var korisnici = (List<Korisnik>)HttpContext.Current.Application["korisnici"];

            var version = System.Environment.Version.ToString();
            var user = (Korisnik)HttpContext.Current.Session["user"];
            foreach (var korisnik in korisnici)
            {
                if (!korisnik.Obrisan)
                {
                    if (korisnik.KorisnickoIme == data["username"].ToString() && korisnik.Lozinka == data["password"].ToString())
                    {
                        if (korisnik.UlogaKorisnika == Uloga.MENADZER)
                        {
                            HttpContext.Current.Session["user"] = (Menadzer)korisnik;
                            return Ok(korisnik.UlogaKorisnika);
                        }
                        else if (korisnik.UlogaKorisnika == Uloga.TURISTA)
                        {
                            if (!((Turista)korisnik).Blokiran)
                            {
                                HttpContext.Current.Session["user"] = (Turista)korisnik;
                                return Ok(korisnik.UlogaKorisnika);
                            }
                        }
                        else if(korisnik.UlogaKorisnika == Uloga.ADMINISTRATOR)
                        {
                            HttpContext.Current.Session["user"] = korisnik;
                            return Ok(korisnik.UlogaKorisnika);
                        }

                        return NotFound();
                    }
                }
            }

            return NotFound();
        }

        [Route("user/Logout")]
        public IHttpActionResult Get()
        {
            HttpContext.Current.Session["user"] = null;
            return Ok();
        }

        [HttpPost]
        [Route("user/Register")]
        public IHttpActionResult RegisterUser(Turista user)
        {
            var korisnici = HttpContext.Current.Application["korisnici"] as List<Korisnik>;
            foreach (var korisnik in korisnici)
            {
                if (korisnik.KorisnickoIme == user.KorisnickoIme)
                {
                    return BadRequest();
                }
            }

            korisnici.Add(user);
            SaveUsers();

            return Created("server", true);
        }


        [HttpPost]
        [Route("admin/Register")]
        public IHttpActionResult Register(Menadzer user)
        {
            if (_TestirajAdmin())
            {
                var korisnici = HttpContext.Current.Application["korisnici"] as List<Korisnik>;
                foreach (var korisnik in korisnici)
                {
                    if (korisnik.KorisnickoIme == user.KorisnickoIme)
                    {
                        return BadRequest();
                    }
                }

                //string date = user.DatumRodjenja.ToString("dd/MM/yyyy");
                //user.DatumRodjenja = DateTime.Parse(date);

                korisnici.Add(user);
                SaveUsers();

                return Created("server", true);
            }
            return Unauthorized();
        }

        [Route("admin/remove/{id}")]
        public IHttpActionResult Delete(string id)
        {
            if (_TestirajAdmin())
            {
                var korisnici = HttpContext.Current.Application["korisnici"] as List<Korisnik>;
                foreach (var korisnik in korisnici)
                {
                    if (korisnik.KorisnickoIme == id)
                    {
                        korisnik.Obrisan = true;
                        SaveUsers();
                        return Ok();
                    }
                }
            }

            return Unauthorized();
        }

        [Route("admin/blokiraj")]
        [HttpPut]
        public IHttpActionResult Blokiraj(BlockRequest req)
        {
            if (_TestirajAdmin())
            {
                var korisnici = HttpContext.Current.Application["korisnici"] as List<Korisnik>;
                foreach (var korisnik in korisnici)
                {
                    if (korisnik.KorisnickoIme == req.Id)
                    {
                        ((Turista)korisnik).Blokiran = true;
                        SaveUsers();
                        return Created("server", true);
                    }
                }
            }

            return Unauthorized();
        }


        private bool _TestirajAdmin()
        {
            var tmp = (Korisnik)HttpContext.Current.Session["user"];
            if (tmp == null)
            {
                return false;
            }
            else
            {
                if (tmp.UlogaKorisnika == Uloga.ADMINISTRATOR)
                {
                    return true;
                }

                return false;
            }
        }

        public static void SaveUsers()
        {

            JsonSerializer serializer = new JsonSerializer();

            List<Korisnik> korisnici = HttpContext.Current.Application["korisnici"] as List<Korisnik>;

            using (StreamWriter sw = new StreamWriter(HttpContext.Current.Server.MapPath(@"~\App_Data\korisnici.json")))
            {
                using (JsonWriter jr = new JsonTextWriter(sw))
                {
                    serializer.Serialize(jr, korisnici);
                }
            }

        }
    }

    public class BlockRequest {
        public string Id { get; set; }

        public BlockRequest() { }
    }

    public class PromenaSifre
    {
        public string StaraSifra { get; set; }
        public string NovaSifra { get; set; }

        public PromenaSifre() { }
    }

    public class PromenaPodataka
    {
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Email { get; set; }

        public PromenaPodataka() { }
    }
}
