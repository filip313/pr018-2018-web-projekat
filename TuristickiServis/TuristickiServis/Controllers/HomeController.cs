﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TuristickiServis.Models;

namespace TuristickiServis.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Administracija()
        {
            var user = (Korisnik)HttpContext.Session["user"];
            if(user != null)
            {
                if(user.UlogaKorisnika == Uloga.ADMINISTRATOR)
                {
                    return View("Admin");
                }
            }

            return View("Index");
        }
    }
}
