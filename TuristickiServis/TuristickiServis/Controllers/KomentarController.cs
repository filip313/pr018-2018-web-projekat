﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TuristickiServis.Models;

namespace TuristickiServis.Controllers
{
    [RoutePrefix("rest")]
    public class KomentarController : ApiController
    {
        [Route("komentari/{naziv}")]
        public IHttpActionResult Get(string naziv)
        {
            List<Komentar> komentari = HttpContext.Current.Application["komentari"] as List<Komentar>;
            List<Komentar> ret = new List<Komentar>();
            var user = HttpContext.Current.Session["user"] as Korisnik;

            foreach (var kom in komentari)
            {
                if (user != null && user.UlogaKorisnika == Uloga.MENADZER)
                {
                    if (kom.aranzman.Naziv == naziv)
                    {
                        ret.Add(kom);
                    }
                }
                else
                {
                    if (kom.aranzman.Naziv == naziv && kom.Odobren)
                    {
                        ret.Add(kom);
                    }
                }
            }


            return Ok(ret);
        }

        [HttpPost]
        [Route("komentar/Napravi")]
        public IHttpActionResult OstaviKomentar()
        {
            var user = HttpContext.Current.Session["user"] as Turista;
            if (user != null && user.UlogaKorisnika == Uloga.TURISTA)
            {
                List<Aranzman> aranzmani = HttpContext.Current.Application["aranzmani"] as List<Aranzman>;
                string naziv = HttpContext.Current.Request["naziv"];
                foreach (var ar in aranzmani)
                {
                    if (ar.Naziv == naziv)
                    {
                        string tekst = HttpContext.Current.Request["text"];
                        double ocena = double.Parse(HttpContext.Current.Request["ocena"]);
                        var komentar = new Komentar();
                        komentar.turista.Ime = user.Ime;
                        komentar.turista.KorisnickoIme = user.KorisnickoIme;
                        komentar.turista.Prezime = user.Prezime;
                        komentar.turista.rezervacije = null;
                        komentar.turista.UlogaKorisnika = user.UlogaKorisnika;
                        komentar.aranzman = ar;
                        komentar.Tekst = tekst;
                        komentar.Ocena = ocena;
                        komentar.Obrisan = false;
                        komentar.Odobren = false;
                        komentar.Id = HttpContext.Current.Request["id"];
                        ((List<Komentar>)HttpContext.Current.Application["komentari"]).Add(komentar);
                        _SaveKomentare();
                        return Ok();
                    }
                }
            }

            return Unauthorized();
        }

        [HttpPut]
        [Route("komentar/Odobri")]
        public IHttpActionResult OdobriKomentar(Odobri o)
        {
            var user = HttpContext.Current.Session["user"] as Menadzer;
            if (user != null && user.UlogaKorisnika == Uloga.MENADZER)
            {
                var komentari = HttpContext.Current.Application["komentari"] as List<Komentar>;
                foreach(var komentar in komentari)
                {
                    if(komentar.Id == o.Id)
                    {
                        komentar.Odobren = true;
                        _SaveKomentare();
                        return Created("komentar", true);
                    }
                }
            }

            return Unauthorized();
        }

        private void _SaveKomentare()
        {
            JsonSerializer serializer = new JsonSerializer();

            List<Komentar> komentari = HttpContext.Current.Application["komentari"] as List<Komentar>;

            using (StreamWriter sw = new StreamWriter(HttpContext.Current.Server.MapPath(@"~\App_Data\komentari.json")))
            {
                using (JsonWriter jr = new JsonTextWriter(sw))
                {
                    serializer.Serialize(jr, komentari);
                }
            }
        }

        public class Odobri
        {
            public string Id { get; set; }
            public Odobri() { }
        }
    }
}
