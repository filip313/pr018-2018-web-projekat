﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TuristickiServis.Models;

namespace TuristickiServis.Controllers
{
    [RoutePrefix("rest")]
    public class SmestajController : ApiController
    {
        [HttpPost]
        [Route("smestaj/NapraviSmestaj")]
        public IHttpActionResult NapraviSmestaj()
        {
            Menadzer menadzer = (Menadzer)HttpContext.Current.Session["user"];
            if (_TestMenadzer(menadzer))
            {
                string naziv = HttpContext.Current.Request["naziv"];

                foreach (var item in menadzer.aranzmani)
                {
                    if (item.Naziv == naziv)
                    {
                        var smestaj = new Smestaj();
                        item.Smestaj = smestaj;
                        item.Smestaj.Naziv = HttpContext.Current.Request["smestaj"];
                        item.Smestaj.TipSmestaja = (TipSmestaja)Int32.Parse(HttpContext.Current.Request["tip"]);
                        item.Smestaj.BrojZvezdica = Int32.Parse(HttpContext.Current.Request["broj"]);
                        item.Smestaj.Bazen = bool.Parse(HttpContext.Current.Request["bazen"]);
                        item.Smestaj.SpaCentar = bool.Parse(HttpContext.Current.Request["spa"]);
                        item.Smestaj.Dostupnost = bool.Parse(HttpContext.Current.Request["dostupno"]);
                        item.Smestaj.Bazen = bool.Parse(HttpContext.Current.Request["bazen"]);
                        UserController.SaveUsers();
                        return Created("smestaj", true);
                    }
                }
                return NotFound();
            }
            return Unauthorized();
        }

        [HttpPost]
        [Route("smestaj/IzmeniSmestaj")]
        public IHttpActionResult IzmeniSmestaj()
        {
            Menadzer menadzer = (Menadzer)HttpContext.Current.Session["user"];
            if (_TestMenadzer(menadzer))
            {
                string naziv = HttpContext.Current.Request["naziv"];


                foreach (var item in menadzer.aranzmani)
                {
                    if (item.Naziv == naziv)
                    {
                        item.Smestaj.Naziv = HttpContext.Current.Request["smestaj"];
                        item.Smestaj.TipSmestaja = (TipSmestaja)Int32.Parse(HttpContext.Current.Request["tip"]);
                        item.Smestaj.BrojZvezdica = Int32.Parse(HttpContext.Current.Request["broj"]);
                        item.Smestaj.Bazen = bool.Parse(HttpContext.Current.Request["bazen"]);
                        item.Smestaj.SpaCentar = bool.Parse(HttpContext.Current.Request["spa"]);
                        item.Smestaj.Dostupnost = bool.Parse(HttpContext.Current.Request["dostupno"]);
                        item.Smestaj.Bazen = bool.Parse(HttpContext.Current.Request["bazen"]);
                        UserController.SaveUsers();
                        return Ok();
                    }
                }
            }
            return Unauthorized();
        }

        [Route("smestaj/ObrisiSmestaj/{aranzman}/{smestaj}")]
        public IHttpActionResult Delete(string aranzman, string smestaj)
        {
            Menadzer menadzer = (Menadzer)HttpContext.Current.Session["user"];
            if (_TestMenadzer(menadzer))
            {
                foreach (var item in menadzer.aranzmani)
                {
                    if (item.Naziv == aranzman)
                    {
                        if(item.KrajPutovanja > DateTime.Now)
                        {
                            foreach(var jed in item.Smestaj.smestajneJedinice)
                            {
                                if (jed.Rezervisan)
                                {
                                    return BadRequest();
                                }
                            }
                        }

                        if (item.Smestaj.Naziv == smestaj)
                        {
                            foreach (var jed in item.Smestaj.smestajneJedinice)
                            {
                                jed.Obrisan = true;
                            }
                            item.Smestaj.Obrisan = true;
                            UserController.SaveUsers();
                            return Ok();
                        }
                    }
                }
            }
            return Unauthorized();
        }

        [HttpGet]
        [Route("smestaj/mojSmestaj/{aranzman}/{smestaj}")]
        public IHttpActionResult GetMojSmestaj(string aranzman, string smestaj)
        {
            var user = HttpContext.Current.Session["user"] as Menadzer;
            if (_TestMenadzer(user))
            {
                foreach(var ar in user.aranzmani)
                {
                    if(ar.Naziv == aranzman)
                    {
                        if(ar.Smestaj.Naziv == smestaj)
                        {
                            return Ok(ar.Smestaj);
                        }
                    }

                    return NotFound();
                }
            }

            return Unauthorized();
        }

        private bool _TestMenadzer(Menadzer men)
        {
            if (men == null || men.UlogaKorisnika != Uloga.MENADZER)
            {
                return false;
            }

            return true;
        }

        public class SmestajIzmena
        {
            public string naziv { get; set; }
            public string smestaj { get; set; }
            public int tip { get; set; }
            public int broj { get; set; }
            public bool bazen { get; set; }
            public bool spa { get; set; }
            public bool dostupno { get; set; }
            public bool wifi { get; set; }

            public SmestajIzmena() { }
        }
    }
}
