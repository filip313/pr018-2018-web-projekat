﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TuristickiServis.Models;

namespace TuristickiServis.Controllers
{
    [RoutePrefix("rest")]
    public class SmestajnaJedinicaController : ApiController
    {
        [HttpPost]
        [Route("smestaj/NapraviJedinicu")]
        public IHttpActionResult NapraviSmestaj()
        {
            Menadzer user = (Menadzer)HttpContext.Current.Session["user"];
            if (_TestMenadzer(user))
            {
                string naziv = HttpContext.Current.Request["naziv"];
                foreach (var item in user.aranzmani)
                {
                    if (item.Naziv == naziv)
                    {
                        int maxId = 0;
                        var nova = new SmestajnaJedinica();
                        nova.Cena = Double.Parse(HttpContext.Current.Request["cena"]);
                        nova.DozvoljenBrojGostiju = Int32.Parse(HttpContext.Current.Request["gosti"]);
                        nova.DozvoljeniLjubimci = bool.Parse(HttpContext.Current.Request["ljubimci"]);
                        if(item.Smestaj.smestajneJedinice != null)
                        {
                            foreach(var jed in item.Smestaj.smestajneJedinice)
                            {
                                if(jed.Id > maxId)
                                {
                                    maxId = jed.Id;
                                }
                            }

                            nova.Id = maxId;
                            item.Smestaj.smestajneJedinice.Add(nova);
                        }
                        else
                        {
                            nova.Id = maxId;
                            var jedinice = new List<SmestajnaJedinica>();
                            jedinice.Add(nova);
                            item.Smestaj.smestajneJedinice = jedinice;
                        }

                        UserController.SaveUsers();
                        return Created("jedinica", true);
                        
                    }
                }
            }
            return Unauthorized();
        }

        [HttpPut]
        [Route("smestaj/IzmeniJedinicu")]
        public IHttpActionResult IzmeniSmestaj()
        {
            Menadzer menadzer = (Menadzer)HttpContext.Current.Session["user"];
            if (_TestMenadzer(menadzer))
            {
                string naziv = HttpContext.Current.Request["naziv"];
                foreach (var item in menadzer.aranzmani)
                {
                    if (item.Naziv == naziv)
                    {
                        string id = HttpContext.Current.Request["id"];

                        foreach(var jed in item.Smestaj.smestajneJedinice)
                        {
                            if (jed.Id.ToString() == id)
                            {
                                jed.Cena = Double.Parse(HttpContext.Current.Request["cena"]);
                                if (!jed.Rezervisan)
                                {
                                    jed.DozvoljenBrojGostiju = Int32.Parse(HttpContext.Current.Request["gosti"]);
                                }
                                jed.DozvoljeniLjubimci = bool.Parse(HttpContext.Current.Request["ljubimci"]);
                                UserController.SaveUsers();
                                return Created("jedinica", true);
                            }
                        }
                    }
                    return NotFound();
                }
            }
            return Unauthorized();
        }

        [Route("jedinica/ObrisiJedinicu/{aranzman}/{smestaj}/{id}")]
        public IHttpActionResult Delete(string aranzman, string smestaj, int id)
        {
            Menadzer menadzer = (Menadzer)HttpContext.Current.Session["user"];
            if (_TestMenadzer(menadzer))
            {
                foreach (var item in menadzer.aranzmani)
                {
                    if (item.Naziv == aranzman)
                    {
                        if (item.Smestaj.Naziv == smestaj)
                        {
                            foreach (var jed in item.Smestaj.smestajneJedinice)
                            {
                                if (jed.Id == id)
                                {
                                    if (item.KrajPutovanja < DateTime.Now)
                                    {
                                        jed.Obrisan = true;
                                        UserController.SaveUsers();
                                        return Ok();
                                    }
                                    else
                                    {
                                        if (!jed.Rezervisan)
                                        {
                                            jed.Obrisan = true;
                                            UserController.SaveUsers();
                                            return Ok();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return Unauthorized();
        }


        [Route("jedinica/rezervisi/{naziv}/{id}")]
        [HttpGet]
        public IHttpActionResult Rezervisi(string naziv, int id)
        {
            var user = HttpContext.Current.Session["user"] as Korisnik;
            if(user != null && user.UlogaKorisnika == Uloga.TURISTA){
                user = user as Turista;
                var aranzmani = HttpContext.Current.Application["aranzmani"] as List<Aranzman>;
                foreach(var aranzman in aranzmani)
                {
                    if(aranzman.Naziv == naziv)
                    {
                        foreach(var jedinica in aranzman.Smestaj.smestajneJedinice)
                        {
                            if(jedinica.Id == id)
                            {
                                jedinica.Rezervisan = true;
                                string rezervacijaId = id + "_" + user.KorisnickoIme + "_";
                                while(rezervacijaId.Length < 15)
                                {
                                    int rand = new Random().Next();
                                    rezervacijaId += rand;
                                }

                                var rez = new Rezervacija() { Id = rezervacijaId, Turista = user.KorisnickoIme, Status = StatusRezervacije.AKTIVNA, Aranzman = aranzman, Smestaj = jedinica, Obrisan = false };
                                ((Turista)user).rezervacije.Add(rez);
                                UserController.SaveUsers();
                                return Ok();
                            }
                        }
                    }
                }

                return NotFound();
            }

            return Unauthorized();
        }

        [Route("jedinica/rezervacije/{username}")]
        public IHttpActionResult Get(string username)
        {
            var user = HttpContext.Current.Session["user"] as Korisnik;
            if(user != null)
            {
                var korisnici = HttpContext.Current.Application["korisnici"] as List<Korisnik>;
                if(user.UlogaKorisnika == Uloga.TURISTA)
                {
                    List<Rezervacija> ret = new List<Rezervacija>();
                    foreach(var kor in korisnici)
                    {
                        if(kor.KorisnickoIme == username)
                        {
                            return Ok(((Turista)kor).rezervacije);
                        }
                    }
                }else if(user.UlogaKorisnika == Uloga.MENADZER)
                {
                    var rezervacije = new List<Rezervacija>();
                    List<string> aranzmani = new List<string>();
                    foreach(var ar in ((Menadzer)user).aranzmani)
                    {
                        aranzmani.Add(ar.Naziv);
                    }

                    foreach(var korisnik in korisnici)
                    {
                        if(korisnik.UlogaKorisnika == Uloga.TURISTA)
                        {
                            foreach(var rez in ((Turista)korisnik).rezervacije)
                            {
                                if (aranzmani.Contains(rez.Aranzman.Naziv))
                                {
                                    rezervacije.Add(rez);
                                }
                            }
                        }
                    }

                    return Ok(rezervacije);
                }

            }

            return Unauthorized();
        }

        [Route("jedinica/otkazi/{id}/{naziv}")]
        [HttpPut]
        public IHttpActionResult Otkazi(string id, string naziv)
        {
            var user = HttpContext.Current.Session["user"] as Korisnik;
            if (user != null && user.UlogaKorisnika == Uloga.TURISTA)
            {
                var korisnici = HttpContext.Current.Application["korisnici"] as List<Korisnik>;
                foreach (var usr in korisnici)
                {
                    if(usr.KorisnickoIme == naziv)
                    {
                        int cnt = 0;
                        foreach(var rez in ((Turista)usr).rezervacije)
                        {
                            if(rez.Id == id)
                            {
                                rez.Status = StatusRezervacije.OTKAZANA;
                                foreach(var kor in korisnici)
                                {
                                    if(kor.UlogaKorisnika == Uloga.MENADZER)
                                    {
                                        foreach(var ar in ((Menadzer)kor).aranzmani)
                                        {
                                            foreach(var jed in ar.Smestaj.smestajneJedinice)
                                            {
                                                if(rez.Smestaj.Id == jed.Id)
                                                {
                                                    jed.Rezervisan = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if(rez.Status == StatusRezervacije.OTKAZANA)
                            {
                                cnt++;
                            }
                        }

                        if(cnt >= 2)
                        {
                            ((Turista)usr).Sumnjiv = true;
                        }
                    }
                }
                UserController.SaveUsers();
                return Created("server", id);
            }

            return Unauthorized();
        }

        [HttpGet]
        [Route("jedinica/mojaJedinica/{naziv}/{id}")]
        public IHttpActionResult MojaJedinica(string naziv, string id)
        {
            var user = HttpContext.Current.Session["user"] as Menadzer;
            if (_TestMenadzer(user))
            {
                foreach(var item in user.aranzmani)
                {
                    if(item.Naziv == naziv)
                    {
                        foreach(var jed in item.Smestaj.smestajneJedinice)
                        {
                            if(jed.Id.ToString() == id)
                            {
                                return Ok(jed);
                            }
                        }
                    }
                }
                return NotFound();
            }

            return Unauthorized();
        }


        private bool _TestMenadzer(Menadzer men)
        {
            if (men == null || men.UlogaKorisnika != Uloga.MENADZER)
            {
                return false;
            }

            return true;
        }
    }
}
