﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TuristickiServis.Models;

namespace TuristickiServis.Controllers
{
    [RoutePrefix("rest")]
    public class AranzmanController : ApiController
    {
        [Route("api")]
        public IEnumerable<Aranzman> Get()
        {
            var korisnici = HttpContext.Current.Application["korisnici"] as List<Korisnik>;
            List<Aranzman> ret = new List<Aranzman>();
            foreach(var korisnik in korisnici)
            {
                if(korisnik.UlogaKorisnika == Uloga.MENADZER)
                {
                    foreach (var ar in ((Menadzer)korisnik).aranzmani)
                    {
                        if(ar.Smestaj != null)
                        {

                            if (ar.Smestaj.Obrisan)
                            {
                                ar.Smestaj = null;
                            }
                            else
                            {
                                var toRemove = new List<SmestajnaJedinica>();
                                foreach(var jed in ar.Smestaj.smestajneJedinice)
                                {
                                    if (jed.Obrisan)
                                    {
                                        toRemove.Add(jed);
                                    }
                                }

                                foreach(var jed in toRemove)
                                {
                                    ar.Smestaj.smestajneJedinice.Remove(jed);
                                }
                            }
                        }
                        if (!ar.Obrisan)
                        {
                            ret.Add(ar);
                        }
                    }
                }
            }

            

            return ret.OrderBy(x => x.PocetakPutovanja);
        }

        [Route("api/mojiAranzmani")]
        [HttpGet]
        public IHttpActionResult MojiAranzmani()
        {
            var user = HttpContext.Current.Session["user"] as Menadzer;
            if(user != null && user.UlogaKorisnika == Uloga.MENADZER)
            {
                var ret = new List<Aranzman>();
                foreach(var ar in user.aranzmani)
                {
                    if (!ar.Obrisan)
                    {
                        ret.Add(ar);
                    }
                }
                return Ok(ret);
            }

            return Unauthorized();
        }


        [HttpGet]
        [Route("api/getAranzman/{naziv}")]
        public IHttpActionResult GetAranzman(string naziv)
        {
            var user = HttpContext.Current.Session["user"] as Menadzer;
            if(user != null && user.UlogaKorisnika == Uloga.MENADZER)
            {
                foreach(var item in user.aranzmani)
                {
                    if(item.Naziv == naziv && !item.Obrisan)
                    {
                        return Ok(item);
                    }
                }
            }

            return Unauthorized();
        }

        [HttpPost]
        [Route("api/NapraviAranzman")]
        public HttpResponseMessage NapraviAranzman()
        {
            Menadzer menadzer = (Menadzer)HttpContext.Current.Session["user"];
            if (_TestMenadzer(menadzer))
            {
                string naziv = HttpContext.Current.Request["Naziv"];
                foreach(var ar in menadzer.aranzmani)
                {
                    if(ar.Naziv == naziv)
                    {
                        return new HttpResponseMessage(HttpStatusCode.NotAcceptable);
                    }
                }
                var novi = new Aranzman();


                novi.Naziv = naziv;
                novi.Tip = (TipAranzmana)Int32.Parse(HttpContext.Current.Request["Tip"]);
                novi.TipPrevoza = (TipPrevoza)Int32.Parse(HttpContext.Current.Request["TipPrevoza"]);
                novi.Destinacija = HttpContext.Current.Request["Destinacija"];
                novi.PocetakPutovanja = DateTime.Parse(HttpContext.Current.Request["PocetakPutovanja"]);
                novi.KrajPutovanja = DateTime.Parse(HttpContext.Current.Request["KrajPutovanja"]);
                novi.Mesto.Adresa = HttpContext.Current.Request["Adresa"];
                novi.Mesto.GeografskaDuzina = HttpContext.Current.Request["Duzina"];
                novi.Mesto.GeografskaSirina = HttpContext.Current.Request["Sirina"];
                novi.VremeNalazenja = DateTime.Parse(HttpContext.Current.Request["VremeNalazenja"]);
                novi.MaksBrojPutnika = Int32.Parse(HttpContext.Current.Request["MaksBrojPutnika"]);
                novi.Opis = HttpContext.Current.Request["Opis"];
                novi.ProgramPutovanja = HttpContext.Current.Request["Program"];
                novi.Obrisan = false;

                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    var poster = HttpContext.Current.Request.Files["file"];
                    if (poster.ContentLength > 0)
                    {
                        var profileName = Path.GetFileName(poster.FileName);
                        string path = "/Images/" + profileName;
                        var comPath = HttpContext.Current.Server.MapPath("/Images/") + profileName;
                        novi.PosterAranzmana = path;
                        poster.SaveAs(comPath);

                    }
                }
                menadzer.aranzmani.Add(novi);
                UserController.SaveUsers();
                return new HttpResponseMessage(HttpStatusCode.Created);
            }

            return new HttpResponseMessage(HttpStatusCode.Unauthorized);
        }

        [Route("api/getIzmenu/{naziv}")]
        [HttpGet]
        public IHttpActionResult ProbajIzmenu(string naziv)
        {
            var user = HttpContext.Current.Session["user"] as Menadzer;
            if(user != null && user.UlogaKorisnika == Uloga.MENADZER)
            {
                foreach(var item in user.aranzmani)
                {
                    if(naziv == item.Naziv)
                    {
                        return Ok(item.Naziv);
                    }
                }
            }

            return Unauthorized();
        }

        [HttpPost]
        [Route("api/IzmeniAranzman")]
        public IHttpActionResult Izmeni()
        {
            Menadzer menadzer = (Menadzer)HttpContext.Current.Session["user"];
            if (_TestMenadzer(menadzer))
            {
                string naziv = HttpContext.Current.Request["Naziv"];
                for (int i = 0; i < menadzer.aranzmani.Count; i++)
                {
                    if (menadzer.aranzmani[i].Naziv == naziv)
                    {
                        menadzer.aranzmani[i].Naziv = naziv;
                        menadzer.aranzmani[i].Tip = (TipAranzmana)Int32.Parse(HttpContext.Current.Request["Tip"]);
                        menadzer.aranzmani[i].TipPrevoza = (TipPrevoza)Int32.Parse(HttpContext.Current.Request["TipPrevoza"]);
                        menadzer.aranzmani[i].Destinacija = HttpContext.Current.Request["Destinacija"];
                        menadzer.aranzmani[i].PocetakPutovanja = DateTime.Parse(HttpContext.Current.Request["PocetakPutovanja"]);
                        menadzer.aranzmani[i].KrajPutovanja = DateTime.Parse(HttpContext.Current.Request["KrajPutovanja"]);
                        menadzer.aranzmani[i].Mesto.Adresa = HttpContext.Current.Request["Adresa"];
                        menadzer.aranzmani[i].Mesto.GeografskaDuzina = HttpContext.Current.Request["Duzina"];
                        menadzer.aranzmani[i].Mesto.GeografskaSirina = HttpContext.Current.Request["Sirina"];
                        menadzer.aranzmani[i].VremeNalazenja = DateTime.Parse(HttpContext.Current.Request["VremeNalazenja"]);
                        menadzer.aranzmani[i].MaksBrojPutnika = Int32.Parse(HttpContext.Current.Request["MaksBrojPutnika"]);
                        menadzer.aranzmani[i].Opis = HttpContext.Current.Request["Opis"];
                        menadzer.aranzmani[i].ProgramPutovanja = HttpContext.Current.Request["Program"];

                        if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                        {
                            var poster = HttpContext.Current.Request.Files["file"];
                            if (poster.ContentLength > 0)
                            {
                                var profileName = Path.GetFileName(poster.FileName);
                                string path = "/Images/" + profileName;
                                var comPath = HttpContext.Current.Server.MapPath("/Images/") + profileName;
                                menadzer.aranzmani[i].PosterAranzmana = path;
                                poster.SaveAs(comPath);

                            }
                        }
                            UserController.SaveUsers();
                            return Created("aranzman", true);
                    }
                }
            }

            return Unauthorized();
        }

        [Route("api/ObrisiAranzman/{naziv}")]
        public IHttpActionResult Delete(string naziv)
        {
            Menadzer menadzer = (Menadzer)HttpContext.Current.Session["user"];
            if (_TestMenadzer(menadzer))
            {
                foreach(var aranzman in menadzer.aranzmani)
                {
                    if(aranzman.Naziv == naziv)
                    {
                        if(aranzman.KrajPutovanja > DateTime.Now)
                        {

                            foreach(var item in aranzman.Smestaj.smestajneJedinice)
                            {
                                if (item.Rezervisan)
                                {
                                    return BadRequest();
                                }
                            }
                        }
                        aranzman.Mesto.Obrisan = true;
                        aranzman.Smestaj.Obrisan = true;
                        foreach(var item in aranzman.Smestaj.smestajneJedinice)
                        {
                            item.Obrisan = true;
                        }
                        aranzman.Obrisan = true;
                        UserController.SaveUsers();
                        return Ok();
                    }
                }
            }

            return Unauthorized();
        }

        private bool _TestMenadzer(Menadzer men)
        {
            if(men == null || men.UlogaKorisnika != Uloga.MENADZER)
            {
                return false;
            }

            return true;
        }
    }
}
